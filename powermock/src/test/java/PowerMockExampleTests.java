import atp.tp.Sun;
import atp.tp.Tyrannosaurus;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PowerMockExampleTests {
    @Test
    void mockSingletonTest_enumFixed() {
        /* Create basic mock */
        Sun sun = mock(Sun.class);
        when(sun.sunshine()).thenReturn("7");

        /* Mock class field */
        Whitebox.setInternalState(Sun.class, "INSTANCE", sun);

        Tyrannosaurus dino = new Tyrannosaurus("drako");

        /* Success */
        assertThrows(
                IllegalArgumentException.class,
                () -> dino.daylife(1, 5),
                "Armageddon has happened!"
        );
    }
}
