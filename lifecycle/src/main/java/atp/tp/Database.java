package atp.tp;

import java.util.LinkedList;

public class Database {
    private static Database instance;
    LinkedList<Data> store;

    private Database() {}

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }

        return instance;
    }

    public void init() {
        if (store == null) {
            store = new LinkedList<>();
        }
    }

    public void clear() {
        store = null;
    }

    public void add(Data data) {
        store.add(data);
    }

    public Data getFirst() {
        return store.getFirst();
    }

    public Data getLast() {
        return store.getLast();
    }

    public int size() {
        return store.size();
    }

    public void pop() {
        if (store != null && !store.isEmpty()) {
            store.pop();
        }
    }
}
