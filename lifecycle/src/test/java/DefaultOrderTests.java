import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DefaultOrderTests {
    static int data = 10;

    @Test
    void first() {
        assertEquals(10, data);
        data = 5;
    }

    @Test
    void second() {
        assertEquals(5, data);
        data = 7;
    }

    @Test
    void third() {
        assertEquals(7, data);
        data = 9;
    }

    @Test
    void fourth() {
        assertEquals(9, data);
        data = 11;
    }

    @Test
    void fifth() {
        assertEquals(11, data);
    }
}
