import atp.tp.Data;
import atp.tp.Database;
import org.junit.jupiter.api.Test;

public class ABreakerTests {
    @Test
    void addGarbage() {
        Database db = Database.getInstance(); // Initialise singleton
        db.init();
        db.add(new Data("garbage")); // Store some garbage
    }
}
