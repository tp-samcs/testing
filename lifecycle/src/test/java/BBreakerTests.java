import atp.tp.Database;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BBreakerTests {
    @Test
    void broken() {
        Database database = Database.getInstance();
        assertEquals("garbage", database.getFirst().getData());
    }
}
