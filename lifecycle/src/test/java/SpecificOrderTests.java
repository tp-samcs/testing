import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

@TestMethodOrder(OrderAnnotation.class)
public class SpecificOrderTests {
    static int data = 10; // Be careful: what will happen if 'static' is removed?

    @Test
    @Order(-1) // Order argument is an integer number. Even negative number
    void first() {
        assertEquals(10, data);
        data = 5;
    }

    @Test
    @Order(0)
    void second() {
        assertEquals(5, data);
        data = 7;
    }

    @Test
    @Order(3)
    void third() {
        assertEquals(7, data);
        data = 9;
    }

    @Test
    @Order(4)
    void fourth() {
        assertEquals(9, data);
        data = 11;
    }

    @Test
    @Order(5)
    void fifth() {
        assertEquals(11, data);
    }
}
