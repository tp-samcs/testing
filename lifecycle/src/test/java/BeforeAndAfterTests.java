import atp.tp.Data;
import atp.tp.Database;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BeforeAndAfterTests {
    static Database database;
    Data data;

    @BeforeAll
    static void setUpAll() {
        database = Database.getInstance();
        database.clear(); // Clear garbage from previous test executions
        database.init(); // Initialise storage in database
    }

    @BeforeEach
    void setUp() {
        /* Repeated code fragment -> move it to setUp() */
        byte[] array = new byte[21];
        new Random().nextBytes(array);
        data = new Data(new String(array, StandardCharsets.UTF_8));
    }

    @Test
    @Order(1)
    void first() {
        /* List is empty => NoSuchElementException is thrown */
        assertThrows(NoSuchElementException.class,() -> database.getFirst());
    }

    @Order(2)
    @RepeatedTest(5) // Repeat test execution 5 times
    void second() {
        database.add(data);
        assertSame(data, database.getLast());
        assertEquals(1, database.size());
    }

    @AfterEach
    void tearDown() {
        database.pop();
    }

    @AfterAll
    static void tearDownAll() {
        database.clear(); // Clear database
    }
}
