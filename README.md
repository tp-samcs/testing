# Тестирование

## Содержание

0. [Предисловие. Устройство JUnit5](#часть-0)
1. [Простые тесты](simpletests/README.md)
2. [Моки](mock/README.md)
3. [Before/After Each/All - управляем циклом выполнения тестов](lifecycle/README.md)

> Этого пока нет, но может быть когда-нибудь и появится..

4. [Параметрические тесты]()
5. [Статический анализ]()
6. [Сбор результатов]()
7. [AssertJ]()
8. [Тестируем Spring]()
9. ???

## Часть 0

Мы будем рассматривать следующий фреймворк тестирования - JUnit 5 jupiter.

Данный фреймворк состоит из следующих зависимостей:

* `junit-jupiter-api` - API для написание тестов
* `junit-jupiter-engine` - реализация рантайма для выполнения тестов
* `junit-jupiter-params` - параметрические тесты

Также есть модуль `junit-vintage-engine`, который позволяет запускать тесты, написанные на более ранних версиях фреймворка.

Так как писать все три зависимости не особо удобно, то есть специальная зависимость: `junit-jupiter`.

Для запуска же мавеном тестов во время фазы `test` нужен плагин `maven-surefire-plugin`.

---
*P.S. Надеюсь, что после всего стало ясно, почему синглетон могут называть антипаттерном :)*