package atp.tp;

import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Function<Double, Double> f = Math::sin;
        double x0 = -8e-10;
        double eps = 0.000_000_000_1;

        double value = f.apply(x0);
        System.out.println(value);

        double small = Math.pow(Math.abs(value), 1.0 / 3.0) * Math.signum(value);
//        small = Math.pow(6 * 1 * eps, 1.0 / 3.0);
        System.out.println(small);
        System.out.println(small * small * small);
        small = Math.exp(Math.log1p(Math.abs(value) - 1) / 3) * Math.signum(value);
        System.out.println(small);
        System.out.println(small * small * small);
        System.out.println(0.1 + 1.3);
    }
}