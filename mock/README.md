# Часть 2

Будем разбираться с моками.
Итак, у нас есть класс `Sun` - это синглетон.

> *Прим.* Лучшие синглетоны пишутся на енамах!

Более того, тестировать тиранозавров никак не получится - `sunshine` буквально выдает случайные данные.

Единственный выход - использовать mockito.

Но сначала рассмотрим более простой случай.

## Простые моки

Попробуем протестировать метод `sayHi()` у Тиранозавра.
Он вызывает метод `echo()` у класса `Echo`, который в свою очередь ожидает ввода части слова.

Попробуем напрямую: сделаем ассерт, в котором проверим результат работы метода.

Заметим, что теперь при запуске тестов мы попали в цикл бесконечность (метод ожидает ввода).

Как решить сложившуюся проблему? Будем подменять ввод с терминала ?
Нет - мы пишем юнит-тесты. Они тестируют конкретный метод, поэтому подменять будем сразу вызов `echo`.

Для этого нам и требуется **Mockito** - еще один фреймворк для тестирования.

Чтобы создать мок на класс нужно написать следующий код:

```java
MyClass myClassMock = Mockito.mock(MyClass.class);
```

После этого мы имеем объект `myClassMock` типа `MyClass`, который пока ничего не умеет делать.

> Попробуйте создать метод у класса `Echo` и далее вызвать у созданного мока. Что произошло?
> Попробуйте вывести результат через `System.out.println`

Далее используя следующую конструкцию можно определить поведение у мока:

```java
// Define method result
Mockito.when(myClassMock.method(..)).thenReturn(...);
// Or declare exception
Mockito.when(myClassMock.method(..)).thenThrow(...);
```

Можно как явно указывать, при каких аргументах, что вызывать, так и использовать `Mockito.any()` - произвольный аргумент.
Получаем своего рода pattern matching.

> Не стоит забывать про `null`. Он может быть валидным аргументом.

## Моки статических методов

До версии 3.4.0 подобные действия были невозможными и надо было использовать PowerMock.
Сейчас же возможность включена во фреймворк. Попробуем ею воспользоваться: нужно сделать мок на синглетон.

Сделать мок на статический метод можно с помощью `mockStatic`. Таким способом можем протестировать обычный синглетон.

Но что будет если попробовать применить подобную технику к синглетону, написанному с помощью енама?
Тут уже средств Mockito не хватает - время использовать черную магию подмены байт-кода: `PowerMock`!

Далее читать [тут](../powermock/README.md)
