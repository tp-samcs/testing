package atp.tp;

import java.util.Scanner;

public class Echo {
    private final String name;

    public Echo(String name) {
        this.name = name;
    }

    public String echo() {
        Scanner scanner = new Scanner(System.in);
        String greeting = scanner.next();

        return "Hello, " + greeting + "! I am " + name;
    }
}
