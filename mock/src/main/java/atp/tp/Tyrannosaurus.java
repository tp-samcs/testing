package atp.tp;

import static atp.tp.Calculus.integral;

public class Tyrannosaurus {
    private String name;
    Echo echo; // package-private to have access in tests

    public Tyrannosaurus(String name) {
        this.name = name;
        this.echo = new Echo(name);
    }

    public double daylife(double dayStart, double dayEnd) {
        String sunpower = Sun.INSTANCE.sunshine();

        return switch (sunpower) {
            case "0" -> integral(x -> x * x * x / 5, dayStart, dayEnd);
            case "1" -> integral(Math::sqrt, dayStart, dayEnd);
            case "2" -> integral(Math::exp, dayStart, dayEnd);
            case "3" -> integral(Math::log10, dayStart, dayEnd);
            case "4" -> integral(Math::atan, dayStart, dayEnd);
            case "5" -> integral(Math::sinh, dayStart, dayEnd);
            case "6" -> integral(x -> 10 / x, dayStart, dayEnd);
            default -> throw new IllegalArgumentException("Armageddon has happened!");
        };
    }

    public double nightlife(double dayStart, double dayEnd) {
        String moonshine = Moon.getInstance().moonshine();

        return switch (moonshine) {
            case "0" -> integral(x -> x * x * x / 5, dayStart, dayEnd);
            case "1" -> integral(Math::sqrt, dayStart, dayEnd);
            case "2" -> integral(Math::exp, dayStart, dayEnd);
            case "3" -> integral(Math::log10, dayStart, dayEnd);
            case "4" -> integral(Math::atan, dayStart, dayEnd);
            case "5" -> integral(Math::sinh, dayStart, dayEnd);
            case "6" -> integral(x -> 10 / x, dayStart, dayEnd);
            default -> throw new IllegalArgumentException("Armageddon has happened!");
        };
    }

    public String sayHi() {
        return "Roarrrr " + echo.echo();
    }
}
