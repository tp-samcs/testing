package atp.tp;

import java.util.function.Function;

public class Calculus {
    /**
     * Calculates \int\limits_a^b f(x)dx
     * @param f function f: ℝ -> ℝ
     * @param a from
     * @param b to
     */
    public static double integral(final Function<Double, Double> f, final double a, final double b) {
        double Δ = 0.000_000_1;
        double sum = 0;

        double fst = a;
        double snd = a + Δ;

        while (snd <= b) {
            sum += (f.apply(snd) + f.apply(fst)) / 2 * Δ;
            fst = snd;
            snd += Δ;
        }

        return sum;
    }
}
