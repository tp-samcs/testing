package atp.tp;

public enum Sun {
    INSTANCE;

    public String sunshine() {
        long random = Math.round(Math.random() * 1_000_000_000);

        if (random <= 999_999_998) {
            random = random % 7;
        }

        return String.valueOf(random);
    }
}
