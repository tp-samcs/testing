package atp.tp;

public class Dinosaur {
    DinoType type; // package-private to have access in tests
    DinoFarm farm; // package-private to have access in tests

    Dinosaur(DinoType type, DinoFarm farm) {
        this.type = type;
        this.farm = farm;
    }

    public int eat(String args) {
        Food food = farm.produce(args);
        int boost;

        return switch (food) {
            case PLANT -> switch (type) {
                case CARNIVORE -> 0;
                case OMNIVORE -> 100;
            };
            case INVERTEBRATES -> switch (type) {
                case CARNIVORE -> 44;
                case OMNIVORE -> -5;
            };
            case MEET -> switch (type) {
                case CARNIVORE -> 1000;
                case OMNIVORE -> -100;
            };
            case WATER -> 10;
        };
    }
}
