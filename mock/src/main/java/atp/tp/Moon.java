package atp.tp;

public class Moon {
    private static Moon instance;

    private Moon() {}

    public static Moon getInstance() {
        if (instance == null) {
            instance = new Moon();
        }

        return instance;
    }

    public String moonshine() {
        long random = Math.round(Math.random() * 1_000_000_000);

        if (random <= 999_999_998) {
            random = random % 7;
        }

        return String.valueOf(random);
    }
}
