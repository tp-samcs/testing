package atp.tp;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.exceptions.misusing.MissingMethodInvocationException;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.contains;
import static org.mockito.Mockito.endsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.startsWith;
import static org.mockito.Mockito.when;

class TyrannosaurusTest {
    @Test
    void echoTest_timeout() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);
        Future<?> future = executor.submit(() -> {
            Tyrannosaurus dino = new Tyrannosaurus("drako");
            assertEquals("Roarrrr ", dino.sayHi()); // Let's try a naive assertion
        });

        // Set timeout
        executor.schedule(() -> future.cancel(true), 10, TimeUnit.SECONDS);

        try {
            future.get(); // Wait for future completion
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            // Ignore: CancellationException is thrown
        }

        assertTrue(future.isCancelled()); // Timeout ..

        executor.shutdownNow();
    }

    @Test
    void echoTest() {
        Echo echo = mock(Echo.class);
        when(echo.echo()).thenReturn("<<mock result>>");

        Tyrannosaurus dino = new Tyrannosaurus("bibi");
        dino.echo = echo;

        assertEquals("Roarrrr <<mock result>>", dino.sayHi());
    }

    @Test
    void eatTest_simple() {
        DinoFarm farm = mock(DinoFarm.class);
        when(farm.produce(contains("ui"))).thenReturn(Food.MEET);
        when(farm.produce(endsWith("rty"))).thenReturn(Food.INVERTEBRATES);
        when(farm.produce(startsWith("asd"))).thenThrow(NoSuchMethodError.class);

        Dinosaur dino = new Dinosaur(DinoType.CARNIVORE, farm);

        assertEquals(1000, dino.eat("gui")); // contains "ui"
        assertEquals(44, dino.eat("qwerty")); // ends with "rty"
        assertThrows(NoSuchMethodError.class, () -> dino.eat("asdfghj")); // starts with "asd"

        // behaviour was not specified => returned null when produce() was called => NPE
        assertThrows(NullPointerException.class, () -> dino.eat("st"));
    }

    @Test
    void eatTest_any() {
        DinoFarm farm = mock(DinoFarm.class);
        when(farm.produce(anyString())).thenReturn(Food.MEET);

        Dinosaur dino = new Dinosaur(DinoType.CARNIVORE, farm);

        assertEquals(1000, dino.eat("gui"));
        assertEquals(1000, dino.eat("qwerty"));
        assertEquals(1000, dino.eat(""));

        assertThrows(NullPointerException.class, () -> dino.eat(null)); // null is a valid argument

        Mockito.reset(farm); // reset everything set before

        when(farm.produce(any())).thenReturn(Food.INVERTEBRATES); // now behaviour for null is specified

        assertEquals(44, dino.eat("gui"));
        assertEquals(44, dino.eat("qwerty"));
        assertEquals(44, dino.eat(""));
        assertEquals(44, dino.eat(null));
    }

    @Test
    void mockSingletonTest_static() {
        // try-with-resources as a good pattern
        try (MockedStatic<Moon> staticMock = mockStatic(Moon.class)) {
            Moon moon = mock(Moon.class);
            when(moon.moonshine()).thenReturn("7");

            staticMock.when(Moon::getInstance).thenReturn(moon); // mock static method

            Tyrannosaurus dino = new Tyrannosaurus("drako");

            assertThrows(
                    IllegalArgumentException.class,
                    () -> dino.nightlife(1, 5),
                    "Armageddon has happened!"
            );
        }

    }

    @Test
    void mockSingletonTest_enumFail() {
        /* Let's try basic mock */
        Sun sun = mock(Sun.class);
        when(sun.sunshine()).thenReturn("7");

        Tyrannosaurus dino = new Tyrannosaurus("drako");

        // WARNING! In rare cases it raises an exception..
        assertDoesNotThrow(() -> dino.daylife(8, 9)); // Doesn't work :(

        reset(sun); // reset everything

        /* Let's try mockStatic */
        try (MockedStatic<Sun> staticMock = mockStatic(Sun.class)) {
            staticMock.when(Sun.INSTANCE::sunshine).thenReturn("7");

            dino.daylife(8, 9);
        } catch (MissingMethodInvocationException e) { // Mockito can't do what we want :(
            System.out.println("\u001B[36m!Mockito fails!\u001B[0m");
            e.printStackTrace();
        }

    }
}